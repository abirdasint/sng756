<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>

<?php
global $base_url;   // Will point to http://www.example.com
global $base_path;  // Will point to at least "/" or the subdirectory where the drupal in installed.

?>

<div id="page-wrapper">

<div id="page">

<!-- Header -->
	<header id="header" class="nonsticky">	
			<div class="section clearfix">
				<!--<a href="#" id="logo"><img src="images/logo.png"/></a>-->
				<?php if ($logo): ?>
				  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				  </a>
				<?php endif; ?>
				<div class="right_block clearfix">	
				<div class="nav_block">			
				<div class="toggle_menu_btn hide_desktop">
					<span></span>
				</div>
				
					<?php print render($page['header_menu']); ?>
				
				</div>
				<div class="nav_right_block">
				<div class="phone_icon"><a href="tel:+6569111111"><i class="icon"></i> <span>+65 6911 1111</span></a></div>
				<!--<div class="account_link hide_mobile"><a href="#">My Account</a></div>-->
				<div class="quote_link hide_mobile"><a href="#">Get A Quote</a></div>
				</div>
				</div>
			</div>
			 
		</header>

<!-- /Header -->
   
   <section id="main-wrapper" class="clearfix">
   <div id="main" class="clearfix">
   <div id="content" class="column">
				<section class="home_block2" id="block2">
				<?php print render($page['featured_video_underneath']); ?>
				</section>
				<section class="home_block1">
				<?php print render($page['homepage_featured_video']); ?>
				</section>
				
				<section class="home_block3 clearfix">
				<?php print render($page['why_life_insurance']); ?>
				</section>					
			
				<section class="home_block4">
					<?php print render($page['why_singapore_life']); ?>
				</section>
				<section class="home_block5">
					<div class="conatiner">
					
						<?php print render($page['featured_news']); ?>
					
					</div>
				</section>
				</div>
   </div>
   </section>


<footer id="footer-wrapper">
			<div class="section">
				<div id="footer-columns" class="clearfix">
					<div class="footer_block1">
						<p>Singapore life is licensed by the Monetary Authority of Singapore and protected by the Policyholder's Protection Fund</p>
					</div>
					<div class="footer_block2">										
					<a href="#" class="quote">		
					<span>Get a Quote</span>
					</a>
					<div class="hide_desktop call"><a href="tel:+6569111111">Call</a></div>	
					<div class="call_block"><i class="icon"></i> <a href="tel:+6569111111">+65 6911 1111</a></div>
									
					</div>
					<div class="footer_block3 clearfix">
					<div class="column">
					<?php print render($page['footer_firstcolumn']); ?>
					</div>
					<div class="column">
					<?php print render($page['footer_secondcolumn']); ?>
					</div>
					<div class="column">
					<?php print render($page['footer_thirdcolumn']); ?>
					</div>
					<div class="column">
						<div class="social_media_block">
							<ul>
								<li><a href="#"><span>Facebook</span></a></li>
								<li><a href="#"><span>Linkedin</span></a></li>
								<li><a href="#"><span>Youtube</span></a></li>
							</ul>
						</div>
					</div>
					
					</div>
					
					<div class="footer_block4">
						<ul>
							<li><a href="<?php echo base_path(); ?>terms-of-use">Terms of Use</a></li>
							<li><a href="<?php echo base_path(); ?>privacy">Privacy Policy</a></li>
							<li><a href="<?php echo base_path(); ?>fair-dealing">Fair Dealing</a></li>
						</ul>
						<p class="copyright">&copy; Singapore Life 2017</p>
					</div>
				</div>
			</div>
		</footer>
		<div class="backToTop">Back to top</div>
	</div>
</div>	
<div class="mobile_menu_backdrop"></div>




  
