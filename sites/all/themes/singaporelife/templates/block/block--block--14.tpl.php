 <div class="coverage_section">
      <div class="container">
      	<h2>Understand Your Coverage</h2>
      	<h3>Learn what coverage other working professionals have. <br/>But first, tell us a little about yourself...</h3>
      	<form method="post" action="">
      	<div class="coverage_form">      		
      			<div class="field_block">
      				<label>I am</label>
      				<div class="field">
      					<select style="width:120px">
      						<option>gender</option>
      						<option value="male">Male</option>
      						<option value="female">Female</option>
      					</select>
      				</div>
      			</div>
      			<div class="field_block">
      				<label>I am</label>
      				<div class="field">
      					<select style="width:100px">
      						<option>age</option>
      						<option value="20">20</option>
      						<option value="30">30</option>
      					</select>
      				</div>
      			</div>
      			<div class="field_block">
      				<label>My annual income is</label>
      				<div class="field">
      					<select style="width:150px">
      						<option>select</option>
      						<option value="20,000">20,000</option>
      						<option value="30,000">30,000</option>
      					</select>
      				</div>
      			</div>
      			      		
      	</div>
      	<div class="btn_block">
      				<div class="btn">
      					<input type="submit" value="Check coverage"/>
      				</div>
      			</div>
      	</form>
      	<div class="coverage_result_container">
     <p class="text-center">Based on your age and income</p>
     <div class="tab_section">
     	<ul>
     		<li data-value="#life-cover" class="active">Life Cover</li>
     		<li data-value="#critical-illness-cover">Critical Illness Cover</li>
     	</ul>
     </div>
     <div class="tab_content_container">
     	<div class="tab_content" id="life-cover">
     	<div class="coverage_box_container">
     		<div class="coverage_box">
     		<i class="icon icon1"></i>
     		<p>Others like you have an average cover of</p>
     		<div class="price">$200,000*</div>
     		</div>
     		<div class="coverage_box">
     		<i class="icon icon2"></i>
     		<p>However, people like you <span>need</span> an average cover of</p>
     		<div class="price">$350,000</div>
     		</div>
     		</div>
     		<p class="text-center">*Based on a survey conducted by Singapore Life.</p>
     		<p class="text-center" style="color:#13abd3">Why is this amount of coverage suggested for you?</p>
     	</div>
     	<div class="tab_content" id="critical-illness-cover"></div>
     </div>
      <div class="btn_block">
      	<a href="#">GET A QUOTE</a>
      </div>
     
      	</div>
		</div>
      </div>