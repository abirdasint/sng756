jQuery(function(){
	var videoUrl = jQuery('.watch_btn a').attr('href');
if(videoUrl == ''){
	jQuery('.watch_btn').remove();
}
	jQuery('.nav_block a').filter(function(){
		return this.hostname && this.hostname !== location.hostname;
	}).attr('target', '_blank');
	
    jQuery("#dd").keyup(function(){
        if(jQuery("#dd").val().length==2 && jQuery.isNumeric(jQuery("#dd").val()))
        jQuery("#mm").focus();
    });
    jQuery("#mm").keyup(function(){
        if(jQuery("#mm").val().length==2 && jQuery.isNumeric(jQuery("#mm").val()))
        jQuery("#yy").focus();
    });
	jQuery(".youtube").colorbox({
		iframe:true, 
		width: '100%',  
		height: '100%',  
		maxWidth: '100%',  
		maxHeight: '100%',
		transition:'none',
		fixed:'true',
        scrolling:'false'   
		});
		
 jQuery(".youtube").colorbox({onOpen: function(){
           jQuery("#colorbox").addClass("fullscreen");
 }});		
jQuery(window).on('load resize orientationchange',function(){
	if(jQuery(window).width() <= 680){
		jQuery(".youtube").colorbox({
		iframe:true, 
		width: '95%',  
		height: '50%',  
		maxWidth: '960px',  
		maxHeight: '960px'
		});
	}
})	
	//banner arrow
	jQuery('.bottom_arrow').click(function(){
		jQuery('body,html').animate({
			scrollTop:jQuery('#block2').position().top
		},2000);
		return false;
	})
//footer menu	
jQuery('.footer_block3 .column').find('.content').addClass('slideUp');
jQuery('.footer_block3 .column h2').click(function(){
	jQuery(this).toggleClass('active').next().toggleClass('slideDown slideUp');
})
var videoblockHeight = jQuery('.video_block').height();
var videoheight = (videoblockHeight-35);

//backto top
jQuery(window).scroll(function(){
	if(jQuery(this).scrollTop() > 300){
		jQuery('.backToTop').fadeIn();
	}else{
		jQuery('.backToTop').fadeOut();
	}
})
jQuery('.backToTop').click(function(){
	jQuery('html,body').animate({scrollTop:0},800);
	return false;
});

//mobile menu toggle
jQuery('.toggle_menu_btn').click(function(){
	if(!jQuery(this).parent().hasClass('open')){
		jQuery(this).parent().addClass('open')
		jQuery('.mobile_menu_backdrop').fadeIn(function(){
			jQuery('.region.region-header-menu').slideDown();
		});
			
	}else{
		jQuery(this).parent().removeClass('open')
		
		jQuery('.region.region-header-menu').slideUp(function(){
			jQuery('.mobile_menu_backdrop').fadeOut();
		});	
	}
	
})
jQuery('.megamenu-parent').removeClass('active');
jQuery('.megamenu-parent').click(function(){
	jQuery(this).toggleClass('active').children('ul').slideToggle();
})

//animation
jQuery('.region-why-life-insurance h2,.region-why-life-insurance .list_item,.region-why-singapore-life h2,.region-why-singapore-life .list_item, .region-featured-news,.footer_block1,.footer_block2, .footer_block3').addClass('animation');
jQuery(window).on('scroll',function(){
		if(jQuery(this).scrollTop() <= 0 ){
			jQuery('.animation').addClass("hidden").removeClass('visible animated fadeInUp');
		}
})
jQuery(window).on('load',function(){
jQuery('.animation').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
	    offset: 100    
});	
})
//banner title
jQuery('#block-views-banner-block .views-field-field-banner-image').find('img').each(function(){
	var imgalt = jQuery(this).attr("alt");
	if(imgalt != ''){
		jQuery('#block-views-banner-block .views-field-field-banner-image').find('.field-content').append('<span class="caption">'+imgalt+'</span>');
	}else{
		//jQuery('#block-views-banner-block .views-field-field-banner-image').find('.field-content').append();
	}
	
});

jQuery('.watchvideo').click(function(){
	jQuery('#youtubevideo').fadeIn();
		jQuery('#homevideoyoutube')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');   

	return false;
})
jQuery(window).on('load resize',function(){
	var windowHeight = jQuery(window).height();
	var windowWidth = jQuery(window).width();
	jQuery("#youtubevideo iframe").width(windowWidth);
    jQuery("#youtubevideo iframe").height(windowHeight);

})
//for textbox label show hide on focus or blur or has val
jQuery('#zendesk-forms-support-form').find('.form-text').each(function(){
	jQuery(this).focus(function(){
		jQuery(this).parent().addClass('focus');
	}).blur(function(){
		if(jQuery(this).val()){
		jQuery(this).parent().addClass('focus');
	}else{
		jQuery(this).parent().removeClass('focus');
	}
	})	
})
/*jQuery('#zendesk-forms-support-form').find('.form-text').each(function(){
	if(jQuery(this).val()){
		jQuery(this).prev('label').fadeOut();
	}else{
		jQuery(this).prev('label').fadeIn();
	}
	
})*/


jQuery('.webform-client-form,.myform').find('.form-text').each(function(){
	jQuery(this).focus(function(){		
		jQuery(this).parent().addClass('focus');
	}).blur(function(){
		if(jQuery(this).val()){
		jQuery(this).parent().addClass('focus');
	}else{
		jQuery(this).parent().removeClass('focus');
	}
	})	
})

//for textarea label show hide on focus or blur or has val
jQuery('#zendesk-forms-support-form').find('.form-textarea').each(function(){
	jQuery(this).focus(function(){		
		jQuery(this).parents('.form-item').addClass('focus');
	}).blur(function(){
		if(jQuery(this).val()){
		jQuery(this).parents('.form-item').addClass('focus');
	}else{
		jQuery(this).parents('.form-item').removeClass('focus');
	}
	})	
})
/*jQuery('#zendesk-forms-support-form').find('.form-textarea').each(function(){
	if(jQuery(this).val()){
		jQuery(this).parents('.form-item').children('label').fadeOut();
	}else{
		jQuery(this).parents('.form-item').children('label').fadeIn();
	}
	
});*/
var pathArray = window.location.pathname.split( '/' );
     var tabno = pathArray[jQuery.inArray( "faq", pathArray )+1];
	 //console.log(parseInt(tabno));
	 if (tabno==1) {
        jQuery( ".view-content" ).accordion( "option", "active",0);
     }
	 if (tabno==2) {
        jQuery( ".view-content" ).accordion( "option", "active",1);
     }
	 if (tabno==3) {
        jQuery( ".view-content" ).accordion( "option", "active",2);
     }
     if (tabno==4) {
        jQuery( ".view-content" ).accordion( "option", "active",3);
     }
//thankyou popup close
jQuery(document).on('click','.after-contactus-submit .close',function(){
	jQuery('.after-contactus-submit').fadeOut(function(){
		jQuery('.mobile_menu_backdrop').fadeOut();
	})
	window.location.reload();
})
jQuery('.product_page_popup,.product .mobile_menu_backdrop').fadeIn();
jQuery(document).on('click','.product_page_popup .close',function(){
	jQuery('.product_page_popup').fadeOut(function(){
		jQuery('.mobile_menu_backdrop').fadeOut();
	})
	
})
//selectbox2
 jQuery('#zendesk-forms-support-form .form-item-subject select,.compare_form select,.coverage_form select').select2({
 	minimumResultsForSearch: -1,
 	dropdownParent: jQuery('#zendesk-forms-support-form .form-item-subject,.compare_form,.coverage_form')
 });	
//for video embeded field trigger 
jQuery('.video-embed-description').click(function(){
	jQuery('.colorbox-load').trigger('click');
})

jQuery('.clime_for_list li').click(function(e) {
    //console.log(jQuery(this).text());
	jQuery('#claim_subject').val(jQuery(this).text());
});
jQuery('#compare').click(function(e) {
    //console.log('hiii');
	if (jQuery('#dd').val().length > 0 && jQuery('#mm').val().length > 0   && jQuery('#yy').val().length > 0 && jQuery('#dd').val() !='DD' && jQuery('#mm').val() !='MM' && jQuery('#yy').val() !='YYYY') {
        console.log('hiii');
		if (jQuery('#dd').val() > 31 || jQuery('#mm').val() > 13 || jQuery('#yy').val().length < 4) {
           jQuery('#doberror').show();
		   return false;
        }
		var agedate = jQuery('#dd').val()+'/'+jQuery('#mm').val()+'/'+jQuery('#yy').val();
		jQuery('#doberror').hide();
		jQuery('#compare-loader').css("display","block");
		jQuery.ajax({
			type: "POST",	
			url:jQuery('#base_url').val()+"/api_curl/compare.php",		       
			data:'gender='+jQuery('#gender').val()+'&somker='+jQuery('#somker').val()+'&pickadate='+agedate,					  
			success: function(data){
				if (data) {
				jQuery('#comparedetails').html(data);
				jQuery('.compare_result_container').show();
				}
		  }
		});
		jQuery('#compare-loader').hide();
    }else{
		jQuery('#doberror').show();
	}
	
});
jQuery( ".pickadate" ).datepicker({
      dateFormat: "dd/mm/yy",
      autoSize: true,
      showOn: "button",
	  changeMonth: true,
      changeYear: true,
	  maxDate: '-20Y',
	  yearRange: "-100:+0",
    });
	jQuery(".linkrd").click(function(){
		window.open(jQuery(this).attr('rel'));
	});
	jQuery('.view-content').bind("DOMSubtreeModified",function(){
		jQuery(".linkrd").click(function(event){
			event.stopImmediatePropagation();
			//console.log('hiiii');
			window.open(jQuery(this).attr('rel'));
		});
	});

jQuery('.more-btn a').click(function(){	
jQuery(this).parent().fadeOut().prev().slideToggle('slow');
jQuery(this).parent().next().fadeIn();
})
jQuery('.less-btn a').click(function(){	
jQuery(this).parent().fadeOut().parents('.product-block').find('.block--more-content').slideToggle('slow',function(){
	jQuery(this).parents('.product-block').find('.more-btn').fadeIn();
});

})
});
function videoclose() {
	jQuery('#homevideoyoutube')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');  
	jQuery('#youtubevideo').fadeOut();
}
  function textareaMaxLength(field, evt, limit,div) {
  //var evt = (evt) ? evt : event;
  //var charCode =
  //  (typeof evt.which != "undefined") ? evt.which :
  // ((typeof evt.keyCode != "undefined") ? evt.keyCode : 0);
  //
  //if (!(charCode >= 13 && charCode <= 126)) {
  //  return true;
  //}
  
  var str = jQuery('#edit-description').val().replace(/^\s+/g, '');
  jQuery('#edit-description').val(str);
  
   var res = field.value.substr(0, 199);
	jQuery(field).val(res);
	var remain = limit-str.length;
	if (remain < 0) {
        remain = 0;
    }
	jQuery('#'+div).html('('+ (remain)+' characters left )');	
}
function testvalidation() {
	//console.log('hiiiii');
	//validation();
	var error = 1;
	if(jQuery('#edit-name').val()==''){
      jQuery('#edit-name').parent().addClass('error');
      jQuery('.name-val').show();
	  error = 2;
    }else{
      jQuery('.name-val').hide();
      jQuery('#edit-name').parent().removeClass('error');
    }
    if(jQuery('#edit-requester').val()==''){
      jQuery('#edit-requester').parent().addClass('error');
      jQuery('.requester-val').html('*Email is required');
       jQuery('.requester-val').show();
	   	error = 2;
    }else{
       var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (!expr.test(jQuery('#edit-requester').val())) {
         jQuery('#edit-requester').parent().addClass('error');
        jQuery('.requester-val').html('*Email is not valid');
        jQuery('.requester-val').show();
		error = 2;
      }else{
        jQuery('#edit-name').parent().removeClass('error');
        jQuery('.requester-val').hide();
      }
      //$('#edit-name').val();
      //$('#edit-requester').val();
    }
	if (error != 1) {
       return false;
    }else{

  jQuery('#edit-submit').trigger('click');
  //alert('hiii');
  //jQuery('.after-contactus-submit').show();
  //  jQuery('.mobile_menu_backdrop').show();
    return ;

	}
	
   
}

