-- SUMMARY --
This module blocks access to all admin pages except from approved IPs. It's default behavior is it will allow log ins and access to the the logged in user's account pages. But you can optionally block those as well, effectively locking all non-anonymous traffic to all but approved IPs.

By default it blocks all the core admin urls, "user/*", "users/*", "node/*/xxx", "taxonomy/*/xxx", "admin/*". Custom and contrib modules SHOULD build their admin interfaces off of one of these patterns but that's by no means guaranteed. The Bean module for example has an admin url for adding beans that does match any core url pattern, "block/add". So there's a configuration page where you can add custom urls.



-- REQUIREMENTS --
None.



-- INSTALLATION --
Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.



-- CONFIGURATION --
When the module is initially enabled, it does nothing. It doesn't become active until you ACTIVATE it on it's admin page, "/admin/config/people/admin_security".

When you activate the module it automatically adds your current IP as the first approved IP. If it didn't do this you would be immediately locked out of your site's administration. You can add as many IPs or IP ranges as you want. You cannot delete your current IP from the list without first deactivating the module. (Deactivate, NOT disable)

Localhost (127.0.0.1) is always allowed and you never have to add it to the list of IPs. The module won't even let you add it. Keep this in mind if you're migrating a localhost installation to a server. If you have this turned on and there are no IPs in the list, you're going to find yourself locked out when you copy the database to your server.

The module will work with single IPs or IP ranges. You can add them in 4 different formats
1. Single IP number: 1.2.3.4
2. Wildcard format: 1.2.3.*
3. CIDR format: 1.2.3/24 OR 1.2.3.4/255.255.255.0
4. Start-End IP format: 1.2.3.0-1.2.3.255



-- TROUBLESHOOTING --
If you do find yourself locked out, you can override the module's settings by entering the following in the settings.php file.

"$conf['admin_security_enabled'] = 0;" deactivates the module.
"$conf['admin_security_login_blocking_enabled'] = 0;" deactivates the optional login blocking only.
"$conf['admin_security_ips'] = array('xxx.xxx.xxx.xxx'); Using this array will OVERRIDE any IPs entered in the UI."


-- LOAD BALANCERS --
On a physical network it’s very easy to get the Client’s IP address on the server-side. But the same code when hosted on Amazon EC2 via an Elastic Load Balancer (ELB) would yield the Private IP address of the EC2 Instance. Some changes might be necessary to get the “true” Client IP address

First, in order to get the Client IP address, the ELB’s Protocol must be set to route at the HTTP layer instead of TCP. If your ELB is already set to route at the TCP layer, you will have to schedule some downtime to create a new ELB that routes HTTP traffic instead and transfer your instances over to that new ELB.

Once that is done, the Client IP address is accessible via the header HTTP_X_FORWARDED_FOR in the client request, which this module will recognize.

NOTE OF CAUTION: This solution does not always work for ELBs that route HTTPS traffic (since they sometimes forward at the TCP layer). The reason is because the HTTP traffic is encrypted using SSL which can be decrypted only at the endpoints. But the ELB cannot perform SSL acceleration and so it cannot get the Client IP address out.

Amazon ELBs however have added support for HTTPS. So you should be able to configure their ELBs to route via HTTPS.

Read more on Load Balancing http://en.wikipedia.org/wiki/Load_balancing_%28computing%29.