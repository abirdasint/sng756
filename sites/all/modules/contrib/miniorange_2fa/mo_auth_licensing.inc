<?php

/**
 * @file
 * Contains support form for miniOrange 2FA Login Module.
 */

/**
 * Showing Support form info.
 */
function mo_auth_licensing($form, &$form_state) {
  global $base_url;
  $license_type = variable_get('mo_auth_2fa_license_type', 'DEMO');
  $license_plan = variable_get('mo_auth_2fa_license_plan', 'DEMO');
  $free = '<div class="mo2f-licensing-thumbnail mo2f-licensing-light-tab">
    <h3 class="mo2f-pricing-header">Free</h3>';
  if ($license_type != 'PREMIUM') {
    $free .= '<h4 style="margin: 5px;padding: 12px 12px 2px 0px;">(You are automatically on this plan)</h4>';
  } else {
    $free .= 'Thank you for upgrading.<br /><br />';
  }
  $free .= '<hr>
    <p>For 1 user - Forever</p><hr>
    <p>$0 - Subscription Fees</p>
    <hr>
    <p style="font-weight:bold">Features:</p>
    <p>
    Limited Authentication Methods,<br>
    Remember Device<br><br><br><br><br><br><br>
    </p><hr>
    <p style="font-weight:bold;">Backup Method:</p>
    <p>Security Questions(KBA)</p><br>
    <hr>
    <p>Basic Support by Email</p>
    </div>';

  $doItYourself = '<div class="mo2f-licensing-thumbnail mo2f-licensing-dark-tab">
    <h3 class="mo2f-pricing-header">Do it yourself</h3>';
  if ($license_type == 'PREMIUM' && strpos($license_plan, 'Do it Yourself') !== false) {
    $doItYourself .= '<h4 style="margin: 5px;padding: 12px 12px 2px 0px;">(You are on this plan)</h4>';
  } elseif ($license_type == 'PREMIUM') {
    $doItYourself .= '<br />';
  } else {
    $doItYourself .= '<input type="submit" data-plan="drupal_2fa_basic_plan" class="mo2f-pricing-button" value="Click here to upgrade" />';
  }
  $doItYourself .= '<hr>
    <p>For 1+ users</p><hr>
    <p>$6 / user / year - Subscription Fees **</p>
    <hr>
    <p style="font-weight:bold">Features:</p>
    <p>
    All Authentication Methods<br>
  Remember Device<br>
  Enforce 2FA registration for users<br>
  Manage Registered Device Profiles<br>
  Custom Redirection<br>
  Customize Email Templates<br>
  Customize SMS Templates<br><br>
    </p><hr>
    <p style="font-weight:bold;">Backup Method:</p>
    <p>Security Questions(KBA)<br>OTP over EMAIL</p>
    <hr>
    <p>Basic Support by Email</p>
    </div>';

  $premium = '<div class="mo2f-licensing-thumbnail mo2f-licensing-light-tab">
    <h3 class="mo2f-pricing-header">Premium</h3>';
if ($license_type == 'PREMIUM' && strpos($license_plan, 'Premium') !== false) {
    $premium .= '<h4 style="margin: 5px;padding: 12px 12px 2px 0px;">(You are on this plan)</h4>';
  } elseif ($license_type == 'PREMIUM') {
    $premium .= 'Thank you for upgrading.<br /><br />';
  } else {
    $premium .= '<input type="submit" data-plan="drupal_2fa_premium_plan" class="mo2f-pricing-button" value="Click here to upgrade" />';
  }

  $premium .= '<hr>
    <p>For 1+ users, Setup and Custom Work</p><hr>
    <p>$6 / user / year - Subscription Fees **</p>
    <hr>
    <p style="font-weight:bold">Features:</p>
    <p>
    All Authentication Methods<br>
  Remember Device<br>
  Enforce 2FA registration for users<br>
  Manage Registered Device Profiles<br>
  Custom Redirection<br>
  Customize Email Templates<br>
  Customize SMS Templates<br>
  End to End 2FA Integration***<br>
    </p><hr>
    <p style="font-weight:bold;">Backup Method:</p>
    <p>Security Questions(KBA)<br>OTP over EMAIL</p>
    <hr>
    <p>Premium Support Plans Available</p>
    </div><br><br>';

  $disclaimer = '<h3>* Steps to upgrade to premium module -</h3>
    <p>1. You will be redirected to miniOrange Login Console. Enter your password with which you created an account with us. After that you will be redirected to payment page.</p>
    <p>2. Enter you card details and complete the payment. On successful payment completion, <a href="?q=admin/config/people/mo_auth">Customer Profile</a> tab and click on Check License button.</p>
    <h3>** Volume discounts are available. Contact Us for details if you have more than 100 users.</h3>
    <p>You can mail us at <a href="mailto:info@miniorange.com"><b>info@miniorange.com</b></a> or submit the query form under Support tab to contact us.</p>
    <h3>*** End to End 2FA Integration - We will setup a Conference Call / Gotomeeting and do end to end setup for you. We provide services to do the setup on your behalf.';

  $form['header']['#markup'] = $free . $doItYourself . $premium . $disclaimer;
  $customer = new MiniorangeCustomerProfile();
  $form['username'] = array (
    '#type' => 'hidden',
    '#value' => $customer->getRegisteredEmail()
  );
  $form['redirectUrl'] = array (
    '#type' => 'hidden',
    '#value' => MoAuthConstants::$BASE_URL . '/initializepayment'
  );
  $form['requestOrigin'] = array (
    '#type' => 'hidden',
    '#value' => ''
  );
  $form['#action'] = MoAuthConstants::$BASE_URL . '/login';
  $form['#attributes'] = array (
    'target' => '_blank'
  );
  return $form;
}