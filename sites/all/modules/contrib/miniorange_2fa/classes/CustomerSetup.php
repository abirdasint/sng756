<?php
/**
 * @file
 * Contains miniOrange Customer class.
 */

/**
 * @file
 * This class represents configuration for customer.
 */
class MiniorangeCustomerSetup {
  public $email;
  public $phone;
  public $customerKey;
  public $transactionId;
  public $password;
  public $otpToken;
  public $defaultCustomerId;
  public $defaultCustomerApiKey;

  /**
   * Constructor.
   */
  public function __construct($email, $phone, $password, $otp_token) {
    $this->email = $email;
    $this->phone = $phone;
    $this->password = $password;
    $this->otpToken = $otp_token;
    $this->defaultCustomerId = MoAuthConstants::$DEFAULT_CUSTOMER_ID;
    $this->defaultCustomerApiKey = MoAuthConstants::$DEFAULT_CUSTOMER_API_KEY;
  }

  /**
   * Check if customer exists.
   */
  public function checkCustomer() {
    $url = MoAuthConstants::$CUSTOMER_CHECK_API;
    $fields = array (
      'email' => $this->email
    );
    $json = json_encode($fields);
    $response = MoAuthUtilities::callService($this->defaultCustomerId, $this->defaultCustomerApiKey, $url, $json);
    if (json_last_error() == JSON_ERROR_NONE && strcasecmp($response->status, 'CURL_ERROR')) {
      $error = array (
        '%method' => 'checkCustomer',
        '%file' => 'CustomerSetup.php',
        '%error' => $response->message
      );
      watchdog('mo_auth', 'Error at %method of %file: %error', $error);
    }
    return $response;
  }

  /**
   * Create Customer.
   */
  public function createCustomer() {
    $url = MoAuthConstants::$CUSTOMER_CREATE_API;

    $fields = array (
      'companyName' => $_SERVER['SERVER_NAME'],
      'areaOfInterest' => MoAuthConstants::$PLUGIN_NAME,
      'email' => $this->email,
      'phone' => $this->phone,
      'password' => $this->password
    );
    $json = json_encode($fields);
    $response = MoAuthUtilities::callService($this->defaultCustomerId, $this->defaultCustomerApiKey, $url, $json);

    if (json_last_error() == JSON_ERROR_NONE && strcasecmp($response->status, 'CURL_ERROR')) {
      $error = array (
        '%method' => 'createCustomer',
        '%file' => 'CustomerSetup.php',
        '%error' => $response->message
      );
      watchdog('mo_auth', 'Error at %method of %file: %error', $error);
    }
    return $response;
  }

  /**
   * Get Customer Keys.
   */
  public function getCustomerKeys() {
    $url = MoAuthConstants::$CUSTOMER_GET_API;

    $fields = array (
      'email' => $this->email,
      'password' => $this->password
    );
    $json = json_encode($fields);

    $response = MoAuthUtilities::callService($this->defaultCustomerId, $this->defaultCustomerApiKey, $url, $json);
    if (json_last_error() == JSON_ERROR_NONE && empty($response->apiKey)) {
      $error = array (
        '%method' => 'getCustomerKeys',
        '%file' => 'CustomerSetup.php',
        '%error' => $response->message
      );
      watchdog('mo_auth', 'Error at %method of %file: %error', $error);
    }
    return $response;
  }

  /**
   * Send OTP.
   */
  public function sendOtp() {
    $url = MoAuthConstants::$AUTH_CHALLENGE_API;

    $username = variable_get('mo_auth_customer_admin_email', NULL);

    $fields = array (
      'customerKey' => $this->defaultCustomerId,
      'email' => $username,
      'authType' => AuthenticationType::$EMAIL['code']
    );
    $json = json_encode($fields);

    $response = MoAuthUtilities::callService($this->defaultCustomerId, $this->defaultCustomerApiKey, $url, $json);

    if (json_last_error() == JSON_ERROR_NONE && strcasecmp($response->status, 'CURL_ERROR')) {
      $error = array (
        '%method' => 'sendOtp',
        '%file' => 'CustomerSetup.php',
        '%error' => $response->message
      );
      watchdog('mo_auth', 'Error at %method of %file: %error', $error);
    }
    return $response;
  }

  /**
   * Validate OTP.
   */
  public function validateOtp($transaction_id) {
    $url = MoAuthConstants::$AUTH_VALIDATE_API;

    $fields = array (
      'txId' => $transaction_id,
      'token' => $this->otpToken
    );
    $json = json_encode($fields);

    $response = MoAuthUtilities::callService($this->defaultCustomerId, $this->defaultCustomerApiKey, $url, $json);

    if (json_last_error() == JSON_ERROR_NONE && strcasecmp($response->status, 'CURL_ERROR')) {
      $error = array (
        '%method' => 'validateOtp',
        '%file' => 'CustomerSetup.php',
        '%error' => $response->message
      );
      watchdog('mo_auth', 'Error at %method of %file: %error', $error);
    }
    return $response;
  }

}

