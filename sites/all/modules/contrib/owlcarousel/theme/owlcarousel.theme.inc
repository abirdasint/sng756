<?php

/**
 * @file
 * owlcarousel.theme.inc
 *
 * Implements theme functions for Owl Carousel.
 */

/**
 * Template process carousel theme.
 */
function template_process_owlcarousel(&$vars) {
  $instance_id = $vars['settings']['instance'];
  $vars['settings']['attributes']['class'][] = $vars['settings']['id'];
}

/**
 * Template preprocess carousel wrapper.
 */
function template_preprocess_owlcarousel_wrapper(&$variables) {
  if (count($variables['output']['#items']) < 2) {
    $variables['output']['#settings']['attributes']['class'][] = 'disabled';
  }
}

/**
 * Theme declaration for Owl Carousel wrapper.
 */
function theme_owlcarousel_wrapper($variables) {
  $attributes = $variables['output']['#settings']['attributes'];

  return '<div' . drupal_attributes($attributes) . '>' . drupal_render($variables['output']) . '</div>';
}

/**
 * Theme declaration for Owl Carousel.
 */
function theme_owlcarousel($variables) {
  $instance = $variables['settings']['id'];
  $settings = $variables['settings'];
  $items = $variables['items'];

  $output = array(
    '#output' => array(
      'settings' => $settings,
      'items' => $items
    ),
    '#pre_render' => array('owlcarousel_pre_render_cache'),
  );

  return drupal_render($output);
}

/**
 * Construct element for render.
 */
function owlcarousel_pre_render_cache($element) {
  $settings = $element['#output']['settings']['instance'];
  $instance = $element['#output']['settings']['id'];

  $output = array(
    '#theme' => 'owlcarousel_list',
    '#items' => $element['#output']['items'],
    '#settings' => $element['#output']['settings']
  );

  // Load carousel settings from the instance id.
  $instance_settings = _owlcarousel_return_carousel_instance_settings($settings);

  // Provide legacy settings alter.
  drupal_alter('owlcarousel_settings', $instance_settings, $instance);

  // Element output /w attached.
  $element['#markup'] = owlcarousel_build($output);
  
  // Provide alter before carousel is rendered.
  drupal_alter('owlcarousel_pre_render', $element);

  $element['#children'] = drupal_render($element['#markup']);
  $element['#attached'] = array(
    'js' => array(
      array(
        'data' => drupal_get_path('module', 'owlcarousel') . '/includes/js/owlcarousel.settings.js',
        'type' => 'file',
        'scope' => 'footer'
      ),
      array(
        'data' => array('owlcarousel' => array(
          $instance => array(
            'settings' => $instance_settings,
            'views' => array(
              'ajax_pagination' => variable_get('owlcarousel_override_ajax_pagination_' . $settings)
            ),
          ),
        ),),
        'type' => 'setting'
      )
    ),
    'library' => array(
      array(
        'owlcarousel',
        'owl-carousel'
      )
    ),
  );

  return $element;
}

/**
 * Build final output array.
 */
function owlcarousel_build($output) {
  return array(
    '#output' => $output,
    '#theme_wrappers' => array('owlcarousel_wrapper'),
  );
}

/**
 * Default theme implementation for lists
 */
function theme_owlcarousel_list(&$vars) {
  $items = &$vars['items'];
  $output = '';
  // Get settings for current instance.
  $settings = _owlcarousel_return_carousel_instance_settings($vars['settings']['instance']);
   $output = '';
  $rows_number = $settings['rowsNumber'];

  if ($rows_number > 1) {
    // @todo: Change this ugly div for more pretty solution.
    // We need to define margin between rows. Have no idea how to do it another way.
    $row_margin = '<div style="margin-top: ' . $settings['rowsMargin'] . 'px;"></div>';
    // We have multi row carousel.
    if ($settings['rowsColumnMode']) {
      // If displaying in column mode, just group each nearest items from array.
      // As example we have items: 1 2 3 4 5 6 7 8
      // It'll be outputed as:
      // 1 3 5 7
      // 2 4 6 8
      $chunks = array_chunk($items, $rows_number);
      $new_items = array();
     foreach ($chunks as $chunk) {
        $item = '';
        // Just build final items which will be wrapped special owlcarousel class.
        foreach ($chunk as $key => $tmp_item) {
          if ($key == $rows_number - 1) {
            $item .= $tmp_item['row'];
          }
          else {
            $item .= $tmp_item['row'] . $row_margin;
          }
        }
        $new_items[] = array('row' => $item);
      }
    }
    else {
      // We want to output items in line mode. As example we need to output it in 3 rows.
      // And we have 13 items. It will outputed like that:
      //  1   2   3   4   5
      //  6   7   8   9
      // 10  11  12  13
      $new_items = array();
      $chunks = array();
      $quantity = $src_quantity = count($items);

      for ($i = 0; $i < $rows_number; $i++) {
        // Recounting on each of stage chunking.
        $length = ceil($quantity / ($rows_number - $i));
        $chunks[] = array_splice($items, 0, $length);
        $quantity -= $length;
      }
      foreach ($chunks as $chunk_key => $chunk) {
        foreach (range(0, floor($src_quantity / $rows_number)) as $current_key) {
          if (!isset($new_items[$current_key])) {
            $new_items[$current_key] = array('row' => '');
          }
          if (isset($chunk[$current_key])) {
            if ($chunk_key != $rows_number - 1) {
              $new_items[$current_key]['row'] .= $chunk[$current_key]['row'] . $row_margin;
            }
            else {
              $new_items[$current_key]['row'] .= $chunk[$current_key]['row'];
            }
         }
        }
      }
    }
    $items = $new_items;
  }
  
  
  
  
  if (!empty($items)) {
    foreach ($items as $i => $item) {
      if ($item['row']) {
        $striping = $i % 2 == 0 ? 'item-odd' : 'item-even';
        $output .= theme('owlcarousel_list_item', array(
          'item' => $item['row'],
          'class' => drupal_html_class('item-' . $i). ' ' . drupal_html_class($striping),
          'group' => $vars['settings']['instance']
        ));
      }
    }
  }

  return $output;
}


/**
 * Default theme implementation for carousel items
 */
function theme_owlcarousel_list_item(&$vars) {
  return '<div class="' . $vars['class'] . '">' . $vars['item'] . '</div>';
}
